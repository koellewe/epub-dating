# EPUB Dater

Determine the earliest possible creation date of an EPUB document by checking its unicode contents.

## Usage

```sh
./age-epub.sh book.epub
```

## Dependencies

Should work in any bash environment (4.2 or later), provided the following commands are available:

- `file`
- `find`
- `unzip`
- `python3` (no special packages required)

## Codepoints

The file `blocks.py` contains an array of deduplicated Unicode blocks and their release versions. The blocks are ordered by codepoints to allow easy binary searching. The file also contains a map of Unicode release dates keyed off by version.  

The script `parse-derived_age.py` created `blocks.py` by parsing the latest Unicode Database `DerivedAge.txt` output ([link](https://www.unicode.org/Public/14.0.0/ucd/DerivedAge.txt)). To run the generator script, you must first download the UCD file and place it in the working directory.
