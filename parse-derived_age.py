# Parses https://www.unicode.org/Public/14.0.0/ucd/DerivedAge.txt into a python array
import copy
import re


blocks = []
version_info = {}


def block_lookup(char_ord, start=0, end=-1):
    if end == -1:
        end = len(blocks)-1
    if start > end:
        return None

    midi = (start+end)//2
    mid = blocks[midi]
    if char_ord < mid[0]:  # search bottom half
        return block_lookup(char_ord, start, midi-1)
    elif char_ord > mid[1]:  # search top half
        return block_lookup(char_ord, midi+1, end)
    else:  # char in midpoint
        return mid, midi


def insert_block(new_block):
    did_insert = False
    for i in range(len(blocks)):
        if new_block[1] < blocks[i][0]:
            blocks.insert(i, new_block)
            did_insert = True
            break

    if not did_insert:
        blocks.append(new_block)


if __name__ == '__main__':
    with open('DerivedAge.txt', 'r') as f:

        loop_version = None  # assigned before use, don't worry
        for line in f:
            if len(line.strip()) == 0:
                continue
            if line.startswith('#'):
                res = re.search(r'[A|a]ssigned [\w\s]+ Unicode (\d{1,3}\.\d{1,3}\.\d{1,3}) \((\w{3,12}), (\d{4})\)', line)
                if res is None:
                    continue
                loop_version, uni_mon, uni_year = res.groups()  # ('1.1.0', 'June', '1993')
                version_info[loop_version] = [uni_mon, int(uni_year),]
                continue

            semi = line.find(';')
            if semi == -1:
                print('Unable to parse line:', line)
                continue

            blob = line[:semi].strip()
            code_range = blob.split('..')
            if len(code_range) == 1:
                start_hex = code_range[0]
                end_hex = start_hex  # one item only
            else:
                start_hex, end_hex = code_range

            start_dec = int(start_hex, 16)
            end_dec = int(end_hex, 16)

            block = [start_dec, end_dec, loop_version,]
            start_lookup = block_lookup(start_dec)
            end_lookup = block_lookup(end_dec)
            if start_lookup is None and end_lookup is None:  # majority case
                insert_block(block)

            elif start_lookup is not None and end_lookup is None:
                old_block_start, osi = start_lookup
                # reduce len of start block and insert new one
                old_block_start[1] = block[0]-1
                if old_block_start[1] < old_block_start[0]:
                    # delete old start block
                    del blocks[osi]
                insert_block(block)
            elif old_block_start is None and end_lookup is not None:
                old_block_end, oei = end_lookup
                # reduce len of end block and insert new one
                old_block_end[0] = block[1]+1
                if old_block_end[0] > old_block_end[1]:
                    # delete old end block
                    del blocks[oei]
                insert_block(block)

            else:  # both os and oe defined.
                old_block_start, osi = start_lookup
                old_block_end, oei = end_lookup
                if old_block_start == old_block_end:  # new block inside one old block
                    new_end_block = copy.deepcopy(old_block_start)
                    new_end_block[0] = block[1] + 1
                    old_block_start[1] = block[0] - 1
                    # check if old_start and new_end have been eliminated
                    if old_block_start[1] < old_block_start[0]:
                        del blocks[osi]
                    insert_block(block)
                    if new_end_block[0] <= new_end_block[1]:
                        insert_block(new_end_block)
                else:  # new block spans two separate old blocks
                    old_block_start[1] = block[0]-1
                    old_block_end[0] = block[1]+1
                    if old_block_end[0] > old_block_end[1]:
                        del blocks[oei]
                    if old_block_start[0] > old_block_start[1]:
                        del blocks[osi]
                    insert_block(block)
    # file close

    # recombine blocks

    for i in range(len(blocks)-2, -1, -1):
        block_i = blocks[i]
        block_down = blocks[i+1]
        if block_i[2] == block_down[2]:
            # merge blocks
            block_i[1] = block_down[1]
            del blocks[i+1]

    # print out
    with open('blocks.py', 'w') as outf:
        print('# auto-generated set of unicode blocks. '
              'Scraped from https://www.unicode.org/Public/14.0.0/ucd/DerivedAge.txt',
              file=outf)
        print('unicode_blocks = [', file=outf)

        for block in blocks:
            print('    ', block, ',', sep='', file=outf)
        print(']', end='\n\n', file=outf)

        print('unicode_releases = {', file=outf)
        for ver, dateinfo in version_info.items():
            print("    '", ver, "': ", dateinfo, ',', sep='', file=outf)
        print('}', file=outf)
