# looks up every character in a file to find its unicode introduction date
# prints out the earliest

from blocks import unicode_blocks, unicode_releases
import sys
import re


def binary_search_block(char_ord, start=0, end=-1):
    """Binary search through unicode blocks to find the one containing the ord"""
    if end == -1:
        end = len(unicode_blocks)-1
    if start > end:
        print('could not find ord', char_ord)
        return None  # should never happen

    midi = (start+end)//2
    mid = unicode_blocks[midi]
    if char_ord < mid[0]:  # search bottom half
        return binary_search_block(char_ord, start, midi-1)
    elif char_ord > mid[1]:  # search top half
        return binary_search_block(char_ord, midi+1, end)
    else:  # char in midpoint
        return mid


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please specify text file')
        exit(1)

    latest_data = []

    with open(sys.argv[1], 'r') as f:
        for line in f:
            # check each char
            for char in line:
                block = binary_search_block(ord(char))
                release = unicode_releases[block[2]]
                if len(latest_data) == 0 or release[1] > latest_data[0][1]:  # compare years
                    latest_data = [
                        release,  # month and year
                        block[2],  # version
                        ord(char),  # decimal unicode char
                    ]

            # check html hash-codes
            for hashcode in re.findall(r'&#(\d{1,10});', line):
                hc = int(hashcode, 10)
                block = binary_search_block(hc)
                release = unicode_releases[block[2]]
                if release[1] > latest_data[0][1]:  # compare years
                    latest_data = [
                        release,  # month and year
                        block[2],  # version
                        hc,  # decimal unicode char
                    ]

    # version;sample_char;year;month
    print(latest_data[1], hex(latest_data[2]).split('x')[-1], latest_data[0][1], latest_data[0][0], sep=';')
