#!/bin/bash

# fail-fast
set -e

STARTING_DIR=$(pwd)

if [[ $# -lt 1 ]]; then
  echo "Provide the epub to scan"
  exit 1
fi

INPUT_FILE=$1

if [[ ! -f $INPUT_FILE ]]; then
  echo "File '$INPUT_FILE' does not exist."
  exit 1
fi

INPUT_TYPE=$(file -b "$INPUT_FILE")

if [[ ! (($INPUT_TYPE == *EPUB\ document*) || ($INPUT_TYPE == *Zip\ data*) || ($INPUT_TYPE == *Zip\ archive\ data*)) ]]; then
  echo "File '$INPUT_FILE' is not of type EPUB or Zip."
  exit 1
fi

WORKSPACE=/tmp/toolmark-analysis
function cleanup(){
  if [ -d "$WORKSPACE" ]; then
      rm -rf "$WORKSPACE"
  fi
}
cleanup

unzip -d $WORKSPACE -qq -o "$INPUT_FILE"
cd $WORKSPACE

UTF_FILES=()
IFS=';'
while read filename; do
  ftype=$(file -ib "$filename")
  if [[ "$ftype" == text/* ]]; then
    UTF_FILES+=("$filename")
  fi  # ignore other ftypes
done < <(find . -type f)  # not creating a subshell

if [[ ${#UTF_FILES[@]} -eq 0 ]]; then
  echo "Found no text subfiles. Earliest creation date is thus October 2007 (EPUB introduction)"
  cleanup
  exit
fi

# version;sample_char;year;month
latest_data=()
for u8file in ${UTF_FILES[*]}; do
  read -r -a res <<< "$(python3 "$STARTING_DIR/uni-lookup.py" "$u8file")"
  if [[ ${#latest_data[@]} -eq 0 || ${res[2]} -gt ${latest_data[2]} ]]; then
    # shellcheck disable=SC2206
    latest_data=(${res[*]})
  fi
done

cleanup

decoded=$(echo -e "\U${latest_data[1]}")

echo "The latest Unicode version used in the document is ${latest_data[0]} (introduced ${latest_data[3]} ${latest_data[2]})"
echo "  Sample char from this version: U+${latest_data[1]} '$decoded'"
if [[ ${latest_data[2]} -lt 2007 ]]; then
  echo "Since EPUB was introduced in Oct 2007, that is the earliest creation date of this document."
else
  echo "Unicode v${latest_data[0]} was introduced in ${latest_data[3]} of ${latest_data[2]}, which is thus the earliest creation date of this document."
fi

